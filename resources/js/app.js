require('./bootstrap');


window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('profile-dropdown', require('./components/ProfileDropdown.vue').default);

const app = new Vue({
    el: "#app",
});