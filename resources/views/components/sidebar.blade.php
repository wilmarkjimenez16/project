<div class="md:w-64 w-16 bg-gray-700 relative  min-h-screen ">
    <div class="bg-gray-800 h-16 flex items-center pl-4">
        <div>
            <font color="white"><strong>Raffle Entries Engine</strong></font>
        </div>
    </div>
    <div class="px-4 pt-4 text-gray-200">
        <div class="">
            <a href="/home" class="-ml-2 px-3 py-2 rounded block hover:bg-gray-800 hover:no-underline hover:text-gray-200">
                <svg class="inline w-4 h-auto mr-1 relative" aria-hidden="true" focusable="false" data-prefix="fas"
                    data-icon="desktop" class="svg-inline--fa fa-desktop fa-w-18" role="img"
                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                    <path fill="currentColor"
                        d="M528 0H48C21.5 0 0 21.5 0 48v320c0 26.5 21.5 48 48 48h192l-16 48h-72c-13.3 0-24 10.7-24 24s10.7 24 24 24h272c13.3 0 24-10.7 24-24s-10.7-24-24-24h-72l-16-48h192c26.5 0 48-21.5 48-48V48c0-26.5-21.5-48-48-48zm-16 352H64V64h448v288z">
                    </path>
                </svg>
                <span class="align-middle">Dashboard</span>
            </a>
            <a href="/raffles" class="-ml-2 px-3 py-2 rounded block hover:bg-gray-800 hover:no-underline hover:text-gray-200">
                <svg class="inline w-4 h-auto mr-1 relative" aria-hidden="true" focusable="false" data-prefix="fas"
                    data-icon="file-import" class="svg-inline--fa fa-file-import fa-w-16" role="img"
                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path fill="currentColor"
                        d="M16 288c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h112v-64zm489-183L407.1 7c-4.5-4.5-10.6-7-17-7H384v128h128v-6.1c0-6.3-2.5-12.4-7-16.9zm-153 31V0H152c-13.3 0-24 10.7-24 24v264h128v-65.2c0-14.3 17.3-21.4 27.4-11.3L379 308c6.6 6.7 6.6 17.4 0 24l-95.7 96.4c-10.1 10.1-27.4 3-27.4-11.3V352H128v136c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H376c-13.2 0-24-10.8-24-24z">
                    </path>
                </svg>
                <span class="align-middle">Raffle Entry</span>
            </a>
            
            <a href="/users/create" class="-ml-2 px-3 py-2 rounded block hover:bg-gray-800 hover:no-underline hover:text-gray-200">
                <svg class="inline w-4 h-auto mr-1 relative" aria-hidden="true" focusable="false" data-prefix="fas"
                    data-icon="file-import" class="svg-inline--fa fa-file-import fa-w-16" role="img"
                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path fill="currentColor"
                        d="M16 288c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h112v-64zm489-183L407.1 7c-4.5-4.5-10.6-7-17-7H384v128h128v-6.1c0-6.3-2.5-12.4-7-16.9zm-153 31V0H152c-13.3 0-24 10.7-24 24v264h128v-65.2c0-14.3 17.3-21.4 27.4-11.3L379 308c6.6 6.7 6.6 17.4 0 24l-95.7 96.4c-10.1 10.1-27.4 3-27.4-11.3V352H128v136c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H376c-13.2 0-24-10.8-24-24z">
                    </path>
                </svg>
                <span class="align-middle">Create User</span>
            </a>
        </div>
    </div>
</div>