<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="h-screen bg-gray-200 text-sm text-gray-600">
    <div id="app">
        <div class="flex">
            @include('components.sidebar')
                <div class="flex-auto py-6 px-12">
                    <header class="">
                    @include('layouts.header')
                </header>
                <main class="mb-12">
                    @yield('content')
                </main>
            </div>
        </div>
    </div>
</body>
</html>
