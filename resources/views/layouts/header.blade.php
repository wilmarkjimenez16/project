<header class="h-16 flex justify-end">
    <div>
        <profile-dropdown :user="{{auth()->user()}}"></profile-dropdown>
    </div>
</header>