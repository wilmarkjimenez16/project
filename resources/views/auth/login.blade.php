<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Cooperative') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <div class="h-screen bg-gray-200 text-sm">
            <div class="container mx-auto h-full flex  justify-center">
                <div class="w-1/2 max-w-sm mx-auto">
                    <div class="mt-16 mb-6 flex justify-center">
                        
                    </div>
                    <div class="py-2 px-4 w-full bg-white rounded shadow-md">
                        <form class="my-4" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="mb-4">
                                <label class="block tracking-wide @error('username') text-red-500 @enderror" for="username">
                                    Username:
                                </label>
                                <input
                                    class="block px-2 py-1 bg-gray-200 shadow-sm rounded w-full @error('username') border border-red-500 @enderror"
                                    name="username" type="text" required>
                                @error('username')
                                <p class="text-red-500">
                                    <strong>{{ $message }}</strong>
                                </p>
                                @enderror

                            </div>
                            <div class="mb-4">
                                <label class="block tracking-wide" for="password">
                                    Password:
                                </label>
                                <input class="block px-2 py-1 bg-gray-200 shadow-sm rounded w-full" type="password"
                                    name="password" required>
                            </div>
                            <div class="">
                                <button
                                    class="appearance-none bg-green-500 hover:bg-green-700 text-white font-semibold py-2 px-4 rounded-lg"
                                    type="submit">
                                    Login
                            </button>
                            </div>
                        </form>
                    </div>
                    <div class="text-xs text-gray-700 text-center py-4">
                        <p>
                            {{'@' . date('Y')}} All rights reserved.
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>

</html>



{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

<div class="card-body">
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                        {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif
            </div>
        </div>
    </form>
</div>
</div>
</div>
</div>
</div>
@endsection --}}