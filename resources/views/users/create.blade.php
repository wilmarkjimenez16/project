@extends('layouts.app')

@section('content')
<div>
    <h1 class="text-2xl text-gray-700">New user</h1>

    <div class="card mt-2">
        <form action="/users" method="post">
            @csrf
            <div class="flex items-center border-b border-gray-200 px-6 pt-6 pb-12">
                <label class="block w-1/4" for="firstname">First name</label>
                <div class="block w-1/2">
                    <input
                        class="w-full focus:outline-none focus:border-2 focus:border-green-400 p-2 rounded-lg border border-gray-400 shadow @error('firstname') border-red-500 text-red-500 @enderror"
                        type="text" name="firstname" placeholder="First name" value="{{old('firstname')}}">
                    @error('firstname')
                    <p class="text-red-500"><strong>{{$message}}</strong></p>
                    @enderror
                </div>

            </div>
            <div class="flex items-center border-b border-gray-200 px-6 pt-6 pb-12">
                <label class="block w-1/4" for="middlename">Middle name</label>
                <div class="block w-1/2">
                    <input
                        class="w-full focus:outline-none focus:border-2 focus:border-green-400 p-2 rounded-lg border border-gray-400 shadow @error('middlename') border-red-500 text-red-500 @enderror"
                        type="text" name="middlename" placeholder="Middle name" value="{{old('middlename')}}">
                    @error('middlename')
                    <p class="text-red-500"><strong>{{$message}}</strong></p>
                    @enderror
                </div>

            </div>
            <div class="flex items-center border-b border-gray-200 px-6 pt-6 pb-12">
                <label class="block w-1/4" for="lastname">Last name</label>
                <div class="block w-1/2">
                    <input
                        class="w-full focus:outline-none focus:border-2 focus:border-green-400 p-2 rounded-lg border border-gray-400 shadow @error('lastname') border-red-500 text-red-500 @enderror"
                        type="text" name="lastname" placeholder="Last name" value="{{old('lastname')}}">
                    @error('lastname')
                    <p class="text-red-500"><strong>{{$message}}</strong></p>
                    @enderror
                </div>

            </div>
            <div class="border-b border-gray-200 px-6 pt-6 pb-12 hidden">
                <div class="flex items-center">
                    <label class="block w-1/4" for="role">Role</label>
                    <select
                        class="block w-1/4 focus:outline-none focus:border-2 focus:border-green-400 rounded-lg border border-gray-400 shadow @error('role') border-red-500 text-red-500 @enderror"
                        name="role" id="">
                        <option value="maker" {{ (old('role') == 'maker' ? 'selected' : '')}}>Maker</option>
                        <option value="authorizer" {{(old('role') == 'authorizer' ? 'selected' : '')}}>Authorizer
                        </option>
                        <option value="sysadmin" {{(old('role') == 'sysadmin' ? 'selected' : '')}}>System Admin</option>
                        <option value="printer" {{(old('role') == 'printer' ? 'selected' : '')}}>Printer</option>
                    </select>
                </div>
                @error('role')
                <p class="text-red-500"><strong>{{$message}}</strong></p>
                @enderror
            </div>
            <div class="flex items-center border-b border-gray-200 px-6 pt-6 pb-12 hidden">
                <label class="block w-1/4" for="username">Username</label>
                <div class="block w-1/2 ">
                    <input
                        class="w-full focus:outline-none focus:border-2 focus:border-green-400 p-2 rounded-lg border border-gray-400 shadow @error('username') border-red-500 text-red-500 @enderror"
                        type="text" name="username" value="user">
                    @error('username')
                    <p class="block text-red-500"><strong>{{$message}}</strong></p>
                    @enderror
                </div>
            </div>
            <div class="flex items-center border-b border-gray-200 px-6 pt-6 pb-12 hidden">
                <label class="block w-1/4" for="password">Password</label>
                <div class="block w-1/2">
                    <input
                        class="w-full focus:outline-none focus:border-2 focus:border-green-400 p-2 rounded-lg border border-gray-400 shadow @error('password') border-red-500 text-red-500 @enderror"
                        type="password" name="password" value="password">
                    <span class="text-gray-500">Your password must 8 characters long, should contain at-least 1
                        Uppercase, 1 Lowercase, 1
                        Numeric and 1 special character.</span>
                    @error('password')
                    <p class="text-red-500"><strong>{{$message}}</strong></p>
                    @enderror
                </div>
            </div>
            <div class="flex items-center border-b border-gray-200 px-6 pt-6 pb-12 hidden">
                <label class="block w-1/4" for="password_confirmation">Confirm Password</label>
                <div class="block w-1/2">
                    <input
                        class=" w-full focus:outline-none focus:border-2 focus:border-green-400 p-2 rounded-lg border border-gray-400 shadow @error('password_confirmation') border-red-500 text-red-500 @enderror"
                        type="password" name="password_confirmation" placeholder="" value="password">
                    @error('password_confirmation')
                    <p class="text-red-500"><strong>{{$message}}</strong></p>
                    @enderror
                </div>
            </div>
            <div class=" flex bg-gray-100 justify-end px-6 pt-6 pb-4">
                <button
                    class="appearance-none bg-green-500 hover:bg-green-700 text-white font-semibold py-2 px-4 rounded-lg"
                    type="submit">
                    Create user
                </button>
            </div>
        </form>
    </div>

</div>

@endsection