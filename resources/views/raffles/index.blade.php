@extends('layouts.app')

@section('content')
<div class="form-row">
    <div class="form-group">
        <h1><strong>List of Names</strong></h1>
        <form action="raffles/{{ $count }}" method="POST">
            @csrf
            @method('patch')
            @foreach($users as $user)
            <div class="form-group">
                {{ $user->firstname . " " . $user->lastname }}
            </div>
            @endforeach
            <input type="hidden" name="called" value="{{ $count }}" />
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Pick</button>
            </div>
        </form>
    </div><br />
    <div class="form-group">
        <h1><strong>Added Names</strong></h1>
        @foreach($added as $item)
        <div class="form-group">
            {{ $item->firstname . " " . $item->lastname }}
        </div>
        @endforeach
    </div>
</div>
@endsection