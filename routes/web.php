<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users', 'UsersController@index')->name('users');
Route::get('users/create', 'UsersController@create')->name('create-user');
Route::post('/users','UsersController@store')->name('store-user');
Route::post('/users/{user}/approve','UsersController@approve')->name('approve-user');


// For raffle entries
Route::get('/raffles', 'RaffleController@index')->name('raffles');
Route::patch('/raffles/{user}', 'RaffleController@update')->name('raffles-update');
Auth::routes();