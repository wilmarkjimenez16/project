<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserUnitTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function append_the_names()
    {
        $user = factory('App\User')->create();
        $name = $user->firstname . ' ' . strtoupper($user->middlename)  . ' ' . $user->lastname;
        $this->assertEquals($name, $user->name);
        $this->assertArrayHasKey('name', $user->toArray());
    }

    /** @test */
    function a_user_has_a_path()
    {
        $user = factory('App\User')->create();
        $this->assertEquals('/users/1', $user->path());
    }
}
