<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function create_a_new_user()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        /*
        $user = factory(User::class)->make();

        $this->post('/users', $user->toArray())
            ->assertStatus(302);
        */

        $user = [
            "firstname" => "Wilmark",
            "middlename" => "S.",
            "lastname" => "Jimenez",
            "email" => "wilmarkjimenez16@gmail.com",
            "role" => "superadmin",
            "username" => "mark",
            "password" => bcrypt("testpass"),
            "password_confirmation" => bcrypt("testpass")
        ];

        $this->assertDatabaseHas('users', ['firstname' => $user['firstname'], 'username' => $user['username']]);
    }


    /** @test */
    public function new_user_requires_name()
    {
        $this->signIn();
        $attributes = factory(User::class)->raw(['firstname' => '', 'middlename' => '', 'lastname' => '']);
        $this->post('/users', $attributes)
            ->assertSessionHasErrors(['firstname', 'middlename', 'lastname']);
    }

    /** @test */
    public function new_user_requires_role()
    {
        $this->signIn();
        $attributes = factory(User::class)->raw(['role' => '']);
        $this->post('/users', $attributes)
            ->assertSessionHasErrors('role');
    }

    /** @test */
    public function user_password_must_a_strong_password()
    {
        $this->signIn();
        $attributes = [
            "firstname" => "Wilmark",
            "middlename" => "S.",
            "lastname" => "Jimenez",
            "email" => "wilmarkjimenez16@gmail.com",
            "role" => "superadmin",
            "username" => "mark",
            "password" => bcrypt("testpass"),
            "password_confirmation" => bcrypt("testpass")
        ];

        $this->post('/users', $attributes)
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function not_admin_cannot_create_users()
    {
        $maker = factory(User::class)->create(['role' => 'maker']);
        $this->signIn($maker);
        $attributes = factory(User::class)->raw();
        $this->post('/users', $attributes)
            ->assertStatus(401);
    }

    /** @test */
    public function admin_visit_create_user_page()
    {
        $this->signIn();
        $this->get('/users/create')
            ->assertSee('New user')
            ->assertStatus(200);
    }

    /** @test */
    public function approved_a_created_user()
    {
        $this->signIn();
        $user = factory(User::class)->create([
            'created_by' => factory(User::class)->create()->id,
        ]);

        $this->post($user->path() . '/approve', [])->assertStatus(302);
        $user = $user->fresh();
        $this->assertEquals(auth()->id(), $user->approved_by);
        $this->assertNotNull($user->approved_at);
    }

    /** @test */
    public function creator_cannot_approve_user()
    {
        $this->signIn();
        $user = factory(User::class)->create([
            'created_by' => auth()->id(),
        ]);

        $this->post($user->path() . '/approve', [])->assertStatus(401);
    }
}
