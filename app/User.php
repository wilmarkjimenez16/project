<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        // 'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNameAttribute()
    {
        return $this->firstname . " " . strtoupper($this->middlename) . " " . $this->lastname;
    }

    public function isAdmin()
    {
        return ($this->role == "sysadmin" || $this->role == "superadmin");
    }

    public function isNotAdmin()
    {
        return !$this->isAdmin();
    }

    public function path()
    {
        return '/users/' . $this->id;
    }

    public function creator()
    {
        return $this->belongTo(User::class, 'created_by');
    }

    /*
    public function isCalled()
    {
        $called = User::all()->called()->with('')
    }
    */
}
