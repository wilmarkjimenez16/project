<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('users.index');
    }

    public function create()
    {
        if(auth()->user()->isNotAdmin())
        {
            return abort(401);
        }
        return view('users.create');
    }

    public function store()
    {
        $attributes = request()->validate([
            'firstname' => 'required|unique:users',
            'middlename' => '',
            'lastname' => 'required|unique:users',
            'role' => 'required',
        //    'email' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);

        User::create($attributes + ['created_by' => auth()->id()]);

        return redirect('/users')->with('success', 'User successfully added.');
    }

    public function approve(User $user)
    {
        if(auth()->user()->is($user->creator))
        {
            abort(401);
        }

        $user->update([
            'approved_by' => auth()->id(),
            'approved_at' => now()
        ]);

        return redirect('/home');
    }
}
