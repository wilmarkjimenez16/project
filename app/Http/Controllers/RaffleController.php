<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RaffleController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = array(
            'users' => User::select('firstname','lastname')->where('called', false)->get(),
            'count' => User::where('called', false)->count(),
            'added' => User::select('firstname','lastname')->where('called', true)->get(),
        );

    //    $colors = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
    //    $rand_keys = array_rand($colors, 1);
    //    $rand_keys = array_rand($colors, 1);
    //    echo $colors[$rand_keys];

        return view('raffles.index')->with($data);
        
    }
    
    public function update(Request $request, $id)
    {
        $num = rand(1, $id);
        $edit = User::find($num);
        $edit->called = true;
        $edit->save();

        return back();
    }
}
